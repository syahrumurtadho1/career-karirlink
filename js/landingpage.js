// section testimoni
$("#carousel-testimoni").each(function () {
    var $this = $(this);
  
    $this
      .on({
        "initialized.owl.carousel": function () {
          $this.find(".item").show();
        },
      })
      .owlCarousel({
        loop: false,
        margin: 10,
        nav: false,
        items: 1,
        dots: false,
        mouseDrag: false,
        touchDrag: false
      });
  });

$("#carousel-testimoni-image").each(function () {
var $this = $(this);

$this
    .on({
    "initialized.owl.carousel": function () {
        $this.find(".item").show();
    },
    })
    .owlCarousel({
    loop: false,
    margin: 10,
    nav: false,
    items: 1,
    dots: false,
    mouseDrag: false,
    touchDrag: false
    });

});

$("#testimoni .cta-action").each(function () {
    $(this).find(".btn-next")
        .click(function () {
            $("#carousel-testimoni").trigger("next.owl.carousel");
            $("#carousel-testimoni-image").trigger("next.owl.carousel");
    });
    $(this).find(".btn-prev")
        .click(function () {
            $("#carousel-testimoni").trigger("prev.owl.carousel");
            $("#carousel-testimoni-image").trigger("prev.owl.carousel");
    });
});